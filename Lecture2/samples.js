var Vector = function(a, b, c) {
	if (a == undefined)
		a = 0;
	if (b == undefined)
		b = 0;
	if (c == undefined)
		c = 0;

	this.x = a;
	this.y = b;
	this.z = c;

	this.add = function(param) {
		if (param instanceof Vector) {
			return new Vector(this.x + param.x, this.y + param.y, this.z
					+ param.z);
		}
		;
		return undefined;
	};

	this.sub = function(param) {
		if (param instanceof Vector) {
			return new Vector(this.x - param.x, this.y - param.y, this.z
					- param.z);
		}
		;
		return undefined;
	};

	this.mult = function(ag) {
		if (typeof (ag) == "number") {
			return new Vector(this.x * ag, this.y * ag, this.z * ag);
		} else if (ag instanceof Vector) {
			return (this.x * ag.x + this.y * ag.y + this.z * ag.z);
		}
		;
		return undefined;
	};

	this.mod = function() {
		return Math.sqrt(this.mult(this));
	};

	this.norm = function() {
		var mod = this.mod();
		return new Vector(this.x / mod, this.y / mod, this.z / mod);
	};

	this.angle = function(param) {
		if (param instanceof Vector) {
			return this.mult(param) / this.mod() / param.mod();
		}
		;
		return undefined;
	};
};
