/**
 * An engine which manage all processes in this program. 
 * Mostly its implements life-cycle of graphical program.
 * 
 * @class Engine
 * @constructor
 */
var Engine = function() {
	this.fps = 30;
	this.running = false;
	this.tick = 0.;
	this.timeoutTick = 0;
	this.elements = [];

	this.run = function() {
		this.running = true;
	};

	this.stop = function() {
		this.running = false;
	};

	this.append = function(element) {
		this.elements.push(element);
		this.elements[this.elements.length - 1].setup();
	};

	// running life-cycle with unnamed function
	(function(self) {
		if (self != undefined) {
			this.self = self;
		}
		;

		if (this.self.running) {
			// Updating elements
			for ( var i = 0; i < this.self.elements.length; 
			i++) {
				this.self.elements[i].update(this.self.tick);
			}
			;

			// Draw elements
			for ( var i = 0; i < this.self.elements.length;
			i++) {
				this.self.elements[i].draw();
			}
			;

			this.self.tick += 1000 / this.self.fps;
		}
		;
		this.self.timeoutTick = setTimeout(arguments.callee,
				1000 / this.self.fps);
	})(this);
};
