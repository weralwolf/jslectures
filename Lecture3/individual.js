var Individual = function(paper) {
	/** System parameters */
	this.__class__ = 'Individual'; 
	// /< Here would be only name of current
	// class

	/** Drawing system parameters */
	this.paper = paper;
	this.circle = undefined;

	/** Movement driving parameters */
	this.velocity = {
		x : NaN,
		y : NaN
	};

	/** Movement properties */
	this.position = {
		x : NaN,
		y : NaN
	};
	
	this.base_position = {
		x : NaN,
		y : NaN
	};
	
	this.base_tick = NaN;

	/**
	 * @brief: Setup individual before anything starts
	 */

	this.color = function(r, g, b) {
		hex = Math.round(((r * 256 + g) * 256 + b) * 256);
		console.log('#' + hex.toString(16));
		return '#' + hex.toString(16);
	};

	this.setup = function() {
		this.circle = this.paper.circle(
				Math.random() * paper.width, 
				Math.random() * paper.height,
				10
				);

		var dx = Math.random() - 0.5, 
		    dy = Math.random() - 0.5;
		var r = Math.sqrt(dx * dx + dy * dy);
		this.velocity = {
			x : dx / r / 30,
			y : dy / r / 30
		};

		this.circle.attr({
			fill : this.color(Math.random(), Math.random(), Math.random())
		});

		this.position.x = this.circle.attr('cx');
		this.position.y = this.circle.attr('cy');

		this.base_position.x = this.position.x;
		this.base_position.y = this.position.y;
	};

	this.update = function(tick) {
		if (this.circle === undefined) {
			console.error("Individual circle undefined. Setup element before.");
			return;
		}
		;

		if (this.base_tick != this.base_tick) {
			this.base_tick = tick;
		}
		;

		var changed = false;
		var local_time = tick - this.base_tick;
		
		this.position.x = this.base_position.x + this.velocity.x * local_time;
		this.position.y = this.base_position.y + this.velocity.y * local_time;

		if (this.position.x < 0) {
			this.position.x *= -1;
			this.velocity.x *= -1;
			changed = true;
		} else if (this.position.x > this.paper.width) {
			this.position.x = 2 * this.paper.width - this.position.x;
			this.velocity.x *= -1;
			changed = true;
		}
		;

		if (this.position.y < 0) {
			this.position.y *= -1;
			this.velocity.y *= -1;
			changed = true;
		} else if (this.position.y > this.paper.height) {
			this.position.y = 2 * this.paper.height - this.position.y;
			this.velocity.y *= -1;
			changed = true;
		}
		;

		if (changed) {
			this.base_position.x = this.position.x;
			this.base_position.y = this.position.y;
			this.base_tick = tick;
		}
		;
	};

	this.draw = function() {
		if (this.circle === undefined) {
			console.error("Individual circle undefined. Setup element before.");
			return;
		}
		;

		this.circle.attr('cx', this.position.x);
		this.circle.attr('cy', this.position.y);
	};
};
